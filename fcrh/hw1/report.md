\begin{center}
    \LARGE{Robotics HW1 - B00902106}
\end{center}

## Part A
I followed the instruction on pdf and downloaded the linux version of ARIA and MobileSim.
At the begining, I was not able to launch MobileSim since it'll show some error about malloc.
Soonly, I found it's caused by the limitation of vitual memory space, so I just change the limit and everything works fine.

## Part B
I use functions in termios.h to implement a *getch* function (wrapped in a class called *reader*).
I then wrote a loop that keeps reading keys to determind next target to go and moving robots using *setVel* and *setRotVel*.
However, I encountered a big problem using this method: The latency of *setVel* and *setRotVel* is too long hence I can't control the robot smoothly. So, I changed the way of controling robot. I map *i*, *k*, *j*, *l* to different types of accelerations and maintain the target speed of left wheel and right wheel. The loop keeps reading keys in blocking mode, and once a key is pressed, I'll update the speed of left wheel and right wheel by calling *setVel2*. Since the absolute value of acceleration speed is not big, I'm able to control the robot more smoothly.

The source code can be found in **control.cpp**.

## Part C
It's included in Part D.

## Part D
By using sonars, I'm able to track obstacles around the robot and prevent my robot from crashing into things. Although you may then implement a robot that follows right hand rules to travel through the maze, it's absolutely not suitable for a routing task.

As a result, I implemented a class called *planner* which can hold a map in memory and plan a better route. *planner* scale down the map by using a constant *scale* that passed to the constructor and provide the interface to mark a positoin as obstacle. It then use these informations to plan a shortest route between the robot and destination. Furthermore, to make the route better, *planner* also consider the times of rotation and the distance between obstacles while planning. It also provide a functionality to reset the destination if it found the origin destination impossible to reach, which is activated in the second test case.

With these two utilitys, I wrote a loop to route the robot. At the begining of each iteration, I'll get the result of sonars and report them to *planner*. I then ask *planner* to plan the next target for my robot. After that, I calculate the required degree to rotate and rotate the robot if the degree is large then some threshold. Finally, once the needed degree of rotation is small enough, I'll make the robot move by setting velocity.

It works pretty good on the first sight. However, while I tried to ask it to route to a far destination, my robot keeps trapped by some small dots on the map. So, I tried to expand the radius of those obstacles to prevent my robot from getting too near to them. The situation becomes better after applying this patch.

The source code can be found in **serious.cpp** and the trails are named as **pd_trail_X.png**. Note that all the codes should be compiled with c++11.

\clearpage

Trails to (8, -9, 0)

![](./pd_trail_1.png)

\clearpage

Trails to (1, 22, $\frac{\pi}{2}$)

![](./pd_trail_2.png)

\clearpage

Trails to (17, 11, $\pi$)

![](./pd_trail_3.png)
