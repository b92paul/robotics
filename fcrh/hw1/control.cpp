#include "Aria.h"

#include <termios.h>

#include <iostream>

using namespace std;

const double PI = acos(-1);
const double EPS = PI / 90;

struct reader {
    int fd;
    termios _old, _new;
    reader(FILE *fin=stdin) : fd(fileno(fin)) {
        tcgetattr(fd, &_old);
        _new = _old;
        _new.c_lflag &= ~(ICANON | ECHO);
        tcsetattr(0, TCSANOW, &_new);
        //fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
    }
    ~reader() {
        tcsetattr(0, TCSANOW, &_old);
    }
    int try_getch() {
        unsigned char ch;
        int ret = read(fd, &ch, 1);
        return ret == 1 ? ch : -1;
    }
};

int main(int argc, char **argv) {

	ArRobot robot;
	ArSonarDevice sonar;

	Aria::init();
	ArSimpleConnector connector(&argc,argv);
	if (!connector.connectRobot(&robot)){
        cerr << "Could not connect to robot.. exiting" << endl;
		Aria::shutdown();
		Aria::exit(1);
	}

	robot.addRangeDevice(&sonar);
	robot.comInt(ArCommands::ENABLE, 1);
	robot.runAsync(false);

	robot.lock();
	robot.setVel(0);
	robot.setRotVel(0);
	robot.unlock();

    reader rd;
    int vel1 = 0, vel2 = 0;
    while (true) {

        int ch = rd.try_getch();
        switch (ch) {
            case 'i':
                vel1 = min(vel1 + 10, 200);
                vel2 = min(vel2 + 10, 200);
                break;
            case 'k':
                vel1 = max(vel1 - 10, -200);
                vel2 = max(vel2 - 10, -200);
                break;
            case 'j':
                vel1 = max(vel1 - 10, -200);
                vel2 = min(vel2 + 10, 200);
                break;
            case 'l':
                vel1 = min(vel1 + 10, 200);
                vel2 = max(vel2 - 10, -200);
                break;
        }

        robot.lock();
        robot.setVel2(vel1, vel2);
        robot.unlock();

    }

	Aria::shutdown();
	Aria::exit(0);

}
