#include "Aria.h"

#include <cassert>
#include <errno.h>

#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <utility>
#include <array>

using namespace std;
using namespace std::rel_ops;

static const double PI = acos(-1);
static const double EPS = PI / 90;

template <class T>
struct bidir_vector {
    vector<T> vfront, vback;
    size_t size() const {
        return vfront.size();
    }
    void resize(int _sz) {
        vfront.resize(_sz);
        vback.resize(_sz);
    }
    T& operator[](int idx) {
        assert(abs(idx) < (int)vfront.size());
        return idx > 0 ? vfront[idx] : vback[-idx];
    }
    const T& operator[](int idx) const {
        assert(abs(idx) < (int)vfront.size());
        return idx > 0 ? vfront[idx] : vback[-idx];
    }
};

struct point {
    int r, c;
    bool operator==(const point &cmp) const {
        return tie(r, c) == tie(cmp.r, cmp.c);
    }
    double dist(const point &obj) const {
        int dr = r - obj.r, dc = c - obj.c;
        return sqrt(dr * dr + dc * dc);
    }
    friend ostream& operator<<(ostream& stm, const point &ref) {
        return stm << "(" << ref.r << ", " << ref.c << ")";
    }
};

struct planner {

    static const int INF = 514514514;
    static const array<point, 4> MOVS;

    struct info {
        int dst;
        bool imp, obs, vis;
        info() : dst(INF), imp(false), obs(false), vis(false) {}
    };

    bool dirty;
    const int scale, obs_rad;
    point tag, rtag;
    bidir_vector<bidir_vector<info>> infos;

    planner(point _tag, int _scale=65, int _obs_rad=3) :
        dirty(true), scale(_scale), obs_rad(_obs_rad), tag(conv(_tag)), rtag(_tag) { bound(tag); }

    void resize(size_t r, size_t c) {
        r = max(r, infos.size());
        infos.resize(r);
        c = max(c, infos[0].size());
        for (int i = -(int)r + 1; i < (int)r; i++)
            infos[i].resize(c);
    }

    info& at(const point &pnt) { return infos[pnt.r][pnt.c]; }
    const info& at(const point &pnt) const { return infos[pnt.r][pnt.c]; }
    point conv(const point &pnt) const { return {(int)floor(double(pnt.r) / scale), (int)floor(double(pnt.c) / scale)}; }
    point rconv(const point &pnt) const { return {pnt.r * scale + scale / 2, pnt.c * scale + scale / 2}; }
    bool bounded(const point &pnt) const { return (int)infos.size() > abs(pnt.r) && (int)infos[0].size() > abs(pnt.c); }
    void bound(const point &pnt) { if (!bounded(pnt)) resize(abs(pnt.r) + 1, abs(pnt.c) + 1); }
    info& bound_at(const point &pnt) { bound(pnt); return at(pnt); }
    bool on_path(const point &pnt) { return bound_at(conv(pnt)).imp; }
    bool at_tag(const point &pnt) { return pnt.dist(rtag) < scale / 3; }

    int safeness(const point &pnt, const int rad=2) {
        int ret = rad + 1;
        for (int dr = -rad; dr <= rad; dr++)
            for (int dc = -rad; dc <= rad; dc++) {
                point npnt{pnt.r + dr, pnt.c + dc};
                if (bounded(npnt) && at(npnt).obs) ret = min(ret, max(abs(dr), abs(dc)));
            }
        return ret;
    }

    bool retarget() {
        cerr << "retarget..." << endl;
        point otag = tag;
        bool done = false;
        for (int d = 1; !done && d <= 10; d++)
            for (int x = -d; !done && x <= d; x++) {
                if (!done) {
                    tag = point{otag.r + x, otag.c + (d - abs(x))}; bound(tag);
                    done = safeness(tag, 3) >= 3 && replan({0, 0});
                }
                if (!done) {
                    tag = point{otag.r + x, otag.c - (d - abs(x))}; bound(tag);
                    done = safeness(tag, 3) >= 3 && replan({0, 0});
                }
            }
        if (done) {
            cerr << "new tag: " << tag << endl;
            rtag = rconv(tag);
        } else {
            cerr << "Can't retarget.." << endl;
            tag = otag;
        }
        return done;
    }

    bool replan(const point &src) {

        dirty = false;
        cerr << "replan" << endl;

        bool need_ext = false;
        int rmax = (int)infos.size() - 1;
        int cmax = (int)infos[0].size() - 1;
        for (int r = -rmax; r <= rmax; r++)
            for (int c = -cmax; c <= cmax; c++) {
                at({r, c}).dst = INF;
                at({r, c}).imp = false;
                need_ext |= ((abs(r) == rmax || abs(c) == cmax) && at({r, c}).obs);
            }
        if (need_ext) resize(infos.size() + 1, infos[0].size() + 1);

        at(tag).dst = 0;
        queue<point> que; que.push(tag);
        while (!que.empty()) {
            point cur = que.front(); que.pop();
            for (auto mov : MOVS) {
                point adj{cur.r + mov.r, cur.c + mov.c};
                if (bounded(adj) && !at(adj).obs && at(adj).dst > at(cur).dst + 1) {
                    at(adj).dst = at(cur).dst + 1;
                    que.push(adj);
                }
            }
        }
        if (at(src).dst == INF) {
            cerr << "Can't replan Q___Q" << endl;
            return false;
        }

        at(tag).imp = true;
        for (point it = src, jt = it, lmov = MOVS[0]; it != tag; it = jt) {
            at(it).imp = true;
            int bst_sfn = 0;
            for (auto mov : MOVS) {
                point adj{it.r + mov.r, it.c + mov.c};
                if (bounded(adj) && at(adj).dst < at(it).dst) bst_sfn = max(bst_sfn, safeness(adj, obs_rad));
            }
            point adj{it.r + lmov.r, it.c + lmov.c};
            if (bounded(adj) && at(adj).dst < at(it).dst && bst_sfn == safeness(adj, obs_rad)) {
                jt = adj;
            } else {
                for (auto mov : MOVS) {
                    adj = point{it.r + mov.r, it.c + mov.c};
                    if (bounded(adj) && at(adj).dst < at(it).dst && bst_sfn == safeness(adj, obs_rad))
                        jt = adj, lmov = mov;
                }
            }
        }
        return true;

    }

    void set_vis(const point &_pnt) {
        bound_at(conv(_pnt)).vis = true;
        bound_at(conv(_pnt)).obs = false;
    }

    void _set_obs(const point &pnt) {
        if (!bound_at(pnt).obs) {
            if (at(pnt).vis) {
                //cerr << "rev: " << pnt << endl;
            } else {
                //cerr << "obs: " << pnt << endl;
                at(pnt).obs = true;
                dirty |= at(pnt).imp;
            }
        }
    }

    bool set_obs(const point &_pnt) {
        point pnt = conv(_pnt), rpnt = rconv(pnt);
        if (max(abs(rpnt.r - _pnt.r), abs(rpnt.c - _pnt.c)) <= scale * 0.4)
            for (int dr = -obs_rad; dr <= obs_rad; dr++)
                for (int dc = -obs_rad; dc <= obs_rad; dc++)
                    _set_obs({pnt.r + dr, pnt.c + dc});
        return dirty;
    }

    point extend(const point &dir, const point &pos) {
        point ret = pos;
        while (true) {
            point npnt{ret.r + dir.r, ret.c + dir.c};
            if (bounded(npnt) && at(npnt).imp) ret = npnt;
            else return ret;
        }
    }

    point next(const point &rpnt) {

        point pnt = conv(rpnt);
        if (pnt == tag) return rtag;

        bound(pnt);
        if (dirty || !at(pnt).imp)
            if (!replan(pnt)) retarget();

        for (auto mov : MOVS) {
            point npnt{pnt.r + mov.r, pnt.c + mov.c};
            if (bounded(npnt) && at(npnt).imp && at(npnt).dst < at(pnt).dst)
                return rconv(extend(mov, npnt));
        }

        cerr << "Oops..." << endl;
        for (auto mov : MOVS) {
            point npnt{pnt.r + mov.r, pnt.c + mov.c};
            if (bounded(npnt) && at(npnt).dst < at(pnt).dst)
                return rconv(extend(mov, npnt));
        }
        cerr << "Don't know where to go Q__Q" << endl;
        throw -1;

    }

    void print(const point &_src, const int rrad=15, const int crad=30) const {
        string str;
        point src = conv(_src);
        int dst = bounded(src) ? at(src).dst : INF;
        for (int dr = -rrad; dr <= rrad; dr++) {
            for (int dc = -crad; dc <= crad; dc++) {
                point npnt{src.r + dr, src.c+ dc};
                if (src == npnt) {
                    str += '@';
                } else if (!bounded(npnt)) {
                    str += '#';
                } else if (at(npnt).imp && at(npnt).dst < dst) {
                    str += '*';
                } else if (at(npnt).obs) {
                    str += 'X';
                } else {
                    str += ' ';
                }
            }
            str += '\n';
        }
        cout << str << src << ", " << dst << endl;
    }

};
const array<point, 4> planner::MOVS{
    point{0, 1}, point{0, -1}, point{1, 0}, point{-1, 0}
};

void route_to(ArRobot &rbt, const point &tag, const int ob_deg, const unsigned scale=60, const unsigned view_dist=2000) {

    const int sonarNum = rbt.getNumSonar();
    planner plnr(tag, scale, 200 / scale);
    point next{(int)rbt.getX(), (int)rbt.getY()}, lcur = next;

    int win = 0;
    while (win < 10) {

        point cur{(int)rbt.getX(), (int)rbt.getY()};
        plnr.set_vis(cur);

        bool update = next.dist(cur) < scale / 3 || !plnr.on_path(cur);
        for (int i = 0; i < sonarNum; i++) {
            ArSensorReading *result = rbt.getSonarReading(i);
            if (result->getRange() <= view_dist) {
                ArPose pos = result->getPose();
                update |= plnr.set_obs({(int)pos.getX(), (int)pos.getY()});
            }
        }
        if (update) {
            point tmp = plnr.next(cur);
            if (next != tmp) {
                next = tmp;
                cerr << "next: " << plnr.conv(next) << " (" << plnr.at(plnr.conv(next)).dst << ")" << endl;
            }
        }

        if (update || cur != lcur) {
            lcur = cur;
            cout << "\e[2J";
            plnr.print(cur, 60, 120);
        }

        double dist = cur.dist(next);
        double obj_arc = atan2(next.c - cur.c, next.r - cur.r);
        double cur_arc = rbt.getTh() / 180.0 * PI;
        double dif_arc = fmod(obj_arc - cur_arc, 2 * PI);
        if (dif_arc > PI) dif_arc -= 2 * PI;
        if (dif_arc < -PI) dif_arc += 2 * PI;

        if (plnr.at_tag(cur)) {
            win = win + 1;
            cerr << win << endl;
        } else {
            win = 0;
        }

        rbt.lock();
        rbt.setVel(min(300.0, (abs(dif_arc) < EPS ? max(30.0, dist) : 0)));
        rbt.setRotVel(dif_arc * 40);
        rbt.unlock();

        ArUtil::sleep(50);

    }

    for (int win = 0; win < 10; ) {
        double dif_deg = fmod(ob_deg - rbt.getTh(), 360);
        if (dif_deg > 180) dif_deg -= 360;
        if (dif_deg <= -180) dif_deg += 360;
        win = (abs(dif_deg) < 1) ? win + 1 : 0;
        cerr << win << " " << dif_deg << endl;
        rbt.lock();
        rbt.setRotVel(dif_deg);
        rbt.unlock();
    }

}

int main(int argc, char **argv) {

	ArRobot robot;
	ArSonarDevice sonar;

	Aria::init();
	ArSimpleConnector connector(&argc,argv);
	if (!connector.connectRobot(&robot)){
        cerr << "Could not connect to robot.. exiting" << endl;
		Aria::shutdown();
		Aria::exit(1);
	}

	robot.addRangeDevice(&sonar);
	robot.comInt(ArCommands::ENABLE, 1);
	robot.runAsync(false);

	robot.lock();
	robot.setVel(0);
	robot.setRotVel(0);
	robot.unlock();

    point tag;
    int ob_deg;
    cin >> tag.r >> tag.c >> ob_deg;
    route_to(robot, tag, ob_deg);

	Aria::shutdown();
	Aria::exit(0);

}
