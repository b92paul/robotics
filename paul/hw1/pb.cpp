#include<cstdio>
#include<termios.h>
#include<Aria.h>
#include<algorithm>
#include<signal.h>
using namespace std;
/* Initialize new terminal i/o settings */
static struct termios old, new1;
void initTermios() {
	tcgetattr(0, &old); /* grab old terminal i/o settings */
	new1 = old; /* make new settings same as old settings */
	new1.c_lflag &= ~ICANON; /* disable buffered i/o */
	new1.c_lflag &=  ~ECHO; /* set echo mode */
	tcsetattr(0, TCSANOW, &new1); /* use these new terminal i/o settings now */
}

/* Restore old terminal i/o settings */
void resetTermios(int num) {
	tcsetattr(0, TCSANOW, &old);
	_exit(0);
}
struct listener{
	listener(){initTermios();}
	~listener(){resetTermios(1);}
};
listener li;
ArRobot robot;
int main(int argc, char **argv){
	ArSonarDevice sonar;
	robot.addRangeDevice(&sonar);
	Aria::init();
	ArSimpleConnector connector(&argc,argv);
	if (!connector.connectRobot(&robot)){
		printf("Could not connect to robot... exiting\n");
		Aria::shutdown();
		Aria::exit(1);
	}
	robot.comInt(ArCommands::ENABLE, 1);
	robot.runAsync(false);
	int nowv=0;
	int nowr=0;
	puts("press w to speed up");
	puts("press s to slow down");
	puts("press d to turn right");
	puts("press a to turn left");
	puts("press q to quit");
	// puts("press q to leave or your terminal will crash!");
	// printf("signal %d\n",SIG_ERR==signal(SIGINT,resetTermios));
	while(true){
		char c = getchar();
		switch(c){
			case 'w':
				nowv=min(nowv+10,250);
				nowr=0;
				robot.setVel(nowv);
				robot.setRotVel(nowr);
				break;
			case 's':
				nowv=max(nowv-10,-50);
				nowr=0;
				robot.setVel(nowv);
				robot.setRotVel(nowr);
				break;
			case 'a':
				nowr=min(nowr+5,50);
				robot.setRotVel(nowr);
				break;
			case 'd':
				nowr=max(nowr-5,-50);
				robot.setRotVel(nowr);
				break;
		}
		if(c=='q')break;
	}
	Aria::shutdown();
	Aria::exit(0);
}
