// by b92paul
#include "Aria.h"
#include <cstdio>
#include <queue>
#include <cstdlib>
#include <cstring>
#include <cmath>
using namespace std;
const int N=400;
#define SIZE_OF_GRID  100
typedef pair<int,int> PII;
#define mk(x,y) make_pair(x,y) 
#define X first
#define Y second
bool mymap[N][N];
int  dis[N][N];
int ox=N/2,oy=N/2;
int nx=0,ny=0;
#define STEP 4
int nextMove[STEP][2]={
	{1,0},{-1,0},{0,1},{0,-1}
};
ArRobot robot;
void updateMe(){
	nx = robot.getX()/SIZE_OF_GRID + ox;
	ny = robot.getY()/SIZE_OF_GRID + oy;
}
void rot(int vel){
	robot.lock();
	robot.setRotVel(vel);
	robot.unlock();}
void go(int vel){
	robot.lock();
	robot.setVel(vel);
	robot.unlock();
}
double r = 40;
const double PI=acos(-1);
double theta(double x,double y){
	return atan2(y,x)/PI*180;
}
const int BLOCK_SIZE = 2;
bool good(int x,int y){
	return (x>=0 && x<N)&&(y>=0 && y<N);
}
void block(int x,int y){
	for(int i=-BLOCK_SIZE;i<=BLOCK_SIZE;i++){
		for(int j=-BLOCK_SIZE;j<=BLOCK_SIZE;j++){
			if(good(ox+x+i,oy+y+j))mymap[ox+x+i][oy+y+j]=true;
		}
	}
}
void detect_delete(){
	for(int i=0;i<robot.getNumSonar();i++){
		ArSensorReading* data = robot.getSonarReading(i);
		if(data->getRange()<3000){
			int x = data->getX()/SIZE_OF_GRID;
			int y = data->getY()/SIZE_OF_GRID;
			block(x,y);
			//printf("sonar %d:%lf %lf %d\n",i,data->getX(),
					//						data->getY(),data->getRange());
		}
		else ;//printf("sonar %d: out of range!!\n",i);
	}
}
#define  RV 25
void goTo(int ix,int iy){
	double x= (double)((ix-ox)*2+1)*SIZE_OF_GRID/2;
	double y= (double)((iy-oy)*2+1)*SIZE_OF_GRID/2;
	printf("target = %lf %lf\n",x,y);
	int idx=1;
	while(idx++){
		detect_delete();
		updateMe();if(nx==ix&&ny==iy){puts("done");break;}
		double dx = x-robot.getX();
		double dy = y-robot.getY();
		if(idx%3000000==0)
			printf("atan:%lf getTh:%lf\n",theta(dx,dy),robot.getTh());
		double diff = theta(dx,dy)-robot.getTh();
		if(diff<0)rot(-RV);
		else rot(RV);
		if(fabs(diff)<2)go(50);
		else go(50);
		if(dx*dx+dy*dy< r)break;
	}
	updateMe();
}
void bfs(int fx,int fy){
	queue<pair<int,int> > Q;
	memset(dis,-1,sizeof(dis));
	dis[fx][fy]=0;
	Q.push(mk(fx,fy));
	while(!Q.empty()){
		PII now = Q.front();Q.pop();
		for(int i=0;i<STEP;i++){
			int nextX = now.X+nextMove[i][0];
			int nextY = now.Y+nextMove[i][1];
			if(good(nextX,nextY) &&dis[nextX][nextY]==-1 && !mymap[nextX][nextY]){
				dis[nextX][nextY]=dis[now.X][now.Y]+1;
				Q.push(mk(nextX,nextY));
			}
		}
	}
	printf("after bfs: %d\n",dis[nx][ny]);
}
void printMap(int r){
	for(int i=max(nx-r,0);i<min(nx+r,N);i++){
		for(int j=max(nx-r,0);j<min(ny+r,N);j++){
			if(mymap[i][j])printf("#");
			else printf(".");
		}
		puts("");
	}
	return;
}
void printBfs(int r){
	for(int i=min(nx-r,0);i<max(nx+r,N);i++){
		for(int j=min(nx-r,0);j<max(ny+r,N);j++){
			if(dis[i][j]<100)printf("%3d",dis[i][j]);
			else printf("  .");
		}
		puts("");
	}
}
void AIWalk(int fx,int fy){
	printf("target %d %d\n",fx,fy);
	printf("now %d %d\n",nx,ny);
	//int t = 5;
	while(true){

		puts("AIWalk");
		updateMe();
		detect_delete();
		bfs(fx,fy);bool walk =false;
		if(dis[nx][ny]==0){puts("I'm here!!!");rot(0);go(0);break;}
		for(int i=0;i<4;i++){
			int nextX=nx+nextMove[i][0], nextY=ny+nextMove[i][1];
			if(good(nextX,nextY)&&dis[nx][ny]>dis[nextX][nextY]){
				printf("move to %d %d\n",nextX,nextY);
				goTo(nextX,nextY);
				walk=true;
				break;
			}
		}
		if(!walk)puts("fuck QQQQQQQ.....")	;
	}
}
int main(int argc, char **argv)
{
	ArSonarDevice sonar;
	robot.addRangeDevice(&sonar);
	Aria::init();
	ArSimpleConnector connector(&argc,argv);
	if (!connector.connectRobot(&robot)){
		printf("Could not connect to robot... exiting\n");
		Aria::shutdown();
		Aria::exit(1);
	}
	robot.comInt(ArCommands::ENABLE, 1);
	robot.runAsync(false);
	double fx,fy;
	printf("input final position:");
	scanf("%lf %lf",&fx,&fy);
	updateMe();
	AIWalk(fx/SIZE_OF_GRID+ox,fy/SIZE_OF_GRID+oy);
	for(int i=0;i<10;i++){
		printf("%f %f %f\n", robot.getX()/1000, robot.getY()/1000, robot.getTh());
		ArUtil::sleep(300);
	}
	Aria::shutdown();
	Aria::exit(0);
}
