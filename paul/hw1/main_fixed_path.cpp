#include "Aria.h"
ArRobot robot;
void rot(int vel){
	robot.setRotVel(vel);
}
void rotate(int a){
	if(a==1){
		float now = robot.getTh();
		robot.lock();
		robot.setRotVel(-50);
		robot.unlock();
		while(robot.getTh()> -65+now){
			ArUtil::sleep(30);
		}
		robot.lock();
		robot.setRotVel(0);
		robot.unlock();
	}
	else{
		float now = robot.getTh();
		robot.lock();
		robot.setRotVel(50);
		robot.unlock();
		while(robot.getTh() <now + 65){
			ArUtil::sleep(30);
		}
		robot.lock();
		robot.setRotVel(0);
		robot.unlock();		
	}
	return;
}
void go(double dis,int dir){ // x+-=dir=01 y+-:dir=23
	if(dir==0){
		float now = robot.getX();
		robot.lock();robot.setVel(500);robot.unlock();
		while(robot.getX()<now+dis){
			ArUtil::sleep(30);
		}
		robot.lock();robot.setVel(0);robot.unlock();
	}
	else{
		float now = robot.getY();
		robot.lock();robot.setVel(500);robot.unlock();
		while(robot.getY()>now+dis){
			ArUtil::sleep(30);
		}
		robot.lock();robot.setVel(0);robot.unlock();
	}
	return;
}
int main(int argc, char **argv){
	ArSonarDevice sonar;
	robot.addRangeDevice(&sonar);
	Aria::init();
	ArSimpleConnector connector(&argc,argv);
	if (!connector.connectRobot(&robot)){
		printf("Could not connect to robot... exiting\n");
		Aria::shutdown();
		Aria::exit(1);
	}
	robot.comInt(ArCommands::ENABLE, 1);
	robot.runAsync(false);
	printf("%f %f %f\n",robot.getX(),robot.getY(),robot.getTh());
	rotate(1);
	ArUtil::sleep(300);
	go(-2900,1);
	ArUtil::sleep(300);
	printf("%f %f %f\n",robot.getX(),robot.getY(),robot.getTh());
	rotate(0);
	go(1100,0);
	rotate(1);
	go(-4900,1);
	rotate(0);
	go(5800,0);
	for(int i=0;i<10;i++){
		printf("%f %f %f\n", robot.getX()/1000, robot.getY()/1000, robot.getTh());
		ArUtil::sleep(300);
	}
	Aria::shutdown();
	Aria::exit(0);
}
