#include <string>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int main(int argc, char** args )
{

    if ( argc < 3 ) {
        cerr << "Usage: " << args[0] << "<team_image_path> <image_path>" << endl;
        return 1;
    }

    const int R = 8;
    const int C = 6;
    const double length = 21;
    const Size patsize(C, R);

    vector<Point3f> world_points;
    vector< vector<Point2f> > vect_corners;
    vector< vector<Point3f> > vect_world_points;

    for (int i = 0; i < R; i++)
        for (int j = 0; j < C; j++)
            world_points.push_back(Point3f(i * length-10, j * length+10, 0));

    Size image_size;
    for (int i = 2; i < argc; i++) {

        vector<Point2f> corners;
        Mat image = imread(args[i]);
        image_size = image.size();

        if (!image.data) {
            cerr << "Can't load image." << endl;
            return 1;
        }

        if (!findChessboardCorners(image, patsize, corners)) {
            cerr << "Pattern not found for " << args[i] << endl;
            return 1;
        } else {
            vect_corners.push_back(corners);
            vect_world_points.push_back(world_points);
            cerr << "Corner of image" << i - 1 << "(" << args[i] << ")" << endl;
        }

    }

    vector<Mat> rvecs, tvecs;
    Mat camera_matrix, dist_coefs;
    calibrateCamera(vect_world_points, vect_corners, image_size, camera_matrix, dist_coefs, rvecs, tvecs);  
    cout << camera_matrix << endl;
		freopen("data/position.data","w",stdout);
		for(int i=0;i<rvecs.size();i++){
			cout << tvecs[i] << endl;
			cout << rvecs[i] << endl;
		}

    for (int i = 2; i < argc; i++) {
        Mat image = imread(args[i]), result;
        undistort(image, result, camera_matrix, dist_coefs);
        imwrite((string("out/")+string(args[i]) + ".new.jpg").c_str(), result);
    }

    Mat image = imread(args[1]), result;
    undistort(image, result, camera_matrix, dist_coefs);
    imwrite((string("out/")+string(args[1]) + ".new.jpg").c_str(), result);
		namedWindow( "Display window", WINDOW_AUTOSIZE );
    imshow( "Display window", image );
    waitKey(0);
    imshow( "Display window", result );
    waitKey(0);

    return 0;
}
