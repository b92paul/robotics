package robotics;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
public class Homework3 {
	
	public static void main( String[] args )
	   {
	      System.loadLibrary( Core.NATIVE_LIBRARY_NAME );
	      File file=new File("object.jpg");
	      JFileChooser chooser = new JFileChooser();
	      FileNameExtensionFilter filter = new FileNameExtensionFilter(
	          "Image files", "jpg", "bmp","png");
	      chooser.setFileFilter(filter);
	      int returnVal = chooser.showOpenDialog(null);
	      if(returnVal == JFileChooser.APPROVE_OPTION) {
	         file=chooser.getSelectedFile();
	      }
	      if(!file.canRead()){
	    	  System.out.println("Image cannot be opened or doesnt exist");
	      }
	      Mat lenaOriginal = Highgui.imread(file.getAbsolutePath(),0);
	      Mat lenaFinal = Highgui.imread(file.getAbsolutePath(),org.opencv.highgui.Highgui.CV_LOAD_IMAGE_COLOR);
	      int height=lenaOriginal.height();
	      int width=lenaOriginal.width();
	      if (!lenaOriginal.empty())
	      {
	    	  System.out.println("Image properties--Height: " + height + " Width: " + width);
	    	  for(int i=0;i<height;i++){
	    		  for(int j=0;j<width;j++){
	    			  if(lenaOriginal.get(i, j)[0]>=128){
	    				  lenaOriginal.put(i, j,255.0 );
	    			  }else{
	    				  lenaOriginal.put(i, j, 0.0);
	    			  }
		    	  }
	    	  }
	    	  Mat lenaComponents = lenaOriginal.clone();
	    	  ArrayList<int[]> label=new ArrayList<int[]>();//label,next
	    	  ArrayList<int[]> eqClass=new ArrayList<int[]>();//eqclass
	    	  ArrayList<int[]> runTable=new ArrayList<int[]>();//row,startcol,endcol,permlabel
	    	  ArrayList<int[]> runTableReference=new ArrayList<int[]>();//rowstart,rowend

	    	  ArrayList<double[]> connectedComponents=new ArrayList<double[]>();//count,xstart,ystart,xend,yend,xSum,ySum//m11,m20,m02
	    	  int cont=0;
	    	  runTable.add(new int[]{-1,-1,-1,-1});//initializing the arraylist to 1 by adding an empty element because the algorithm relies on the indexes starting at 1
	    	  label.add(new int[]{-1,-1});//initializing the arraylist to 1 by adding an empty element because the algorithm relies on the indexes starting at 1
	    	  eqClass.add(new int[]{-1});//initializing the arraylist to 1 by adding an empty element because the algorithm relies on the indexes starting at 1
	    	  connectedComponents.add(new double[]{0,-1,-1,-1,-1,0,0,0,0,0});//initializing the arraylist to 1 by adding an empty element because the algorithm relies on the indexes starting at 1
	    	  for(int i=0;i<height;i++){
	    		  for(int j=0;j<width;j++){
	    			  
	    			  if(lenaOriginal.get(i, j)[0]==255){
	    				  
	    				  if(cont==0){
	    					  runTable.add(new int[]{i+1,j+1,j+1,0});//adding the element with startcol = endcol 
	    					  
	    		    		  eqClass.add(new int[]{0});
	    		    		  label.add(new int[]{0,0});
	    		    		  connectedComponents.add(new double[]{0,-1,-1,-1,-1,0,0,0,0,0});
	    					  cont=1;
	    				  }else{
	    					  runTable.get(runTable.size()-1)[2]=j+1;//if the run continues extend the endcol
	    				  }
	    			  }else{
	    				  cont=0;
	    			  }
		    	  }
	    		  cont=0;
	    	  }
	    	  
	    	  int cont2=1;
	    	  runTableReference.add(new int[]{-1,-1});//initializing the arraylist to 1 by adding an empty element because the algorithm relies on the indexes starting at 1
	    	  
	    	  for(int i=1;i<=height;i++){
	    		  int start=0,end=0;
	    		  if(cont2>=runTable.size()){
	    			  break;
	    		  }
	    		  while(runTable.get(cont2)[0]==i){
	    			  if(start==0){
	    				  start=cont2;
	    				  end=cont2;
	    			  }else{
	    				  end=cont2;
	    			  }
	    			  cont2++;
	    			  if(cont2>=runTable.size()){
		    			  break;
		    		  }
	    		  }
	    		  runTableReference.add(new int[]{start,end});//adding the run table rowstart and rowend to the reference table
	    	  }
	    	  runLengthImplementation( runTable,runTableReference,label,eqClass);//call run length implementation to find connected components
	    	  
	    	  for(int i=1;i<runTable.size();i++){
	    		  connectedComponents.get(runTable.get(i)[3])[0]=connectedComponents.get(runTable.get(i)[3])[0]+(runTable.get(i)[2]-runTable.get(i)[1])+1;//calculating the area of the region
	    		  connectedComponents.get(runTable.get(i)[3])[5]=connectedComponents.get(runTable.get(i)[3])[5]+((((runTable.get(i)[2]-1)*(runTable.get(i)[2]))/2)-(((runTable.get(i)[1]-1)*(runTable.get(i)[1]-2))/2));//calculating the xsum
	    		  connectedComponents.get(runTable.get(i)[3])[6]=connectedComponents.get(runTable.get(i)[3])[6]+(((runTable.get(i)[2]-1)-(runTable.get(i)[1]-1))+1)*(runTable.get(i)[0]-1);//calculating the ysum
	    		  if(connectedComponents.get(runTable.get(i)[3])[1]<0 || connectedComponents.get(runTable.get(i)[3])[1] >runTable.get(i)[0]){
	    			  connectedComponents.get(runTable.get(i)[3])[1]=runTable.get(i)[0];//calculate bounding box left x
	    		  }
	    		  if(connectedComponents.get(runTable.get(i)[3])[2]<0 || connectedComponents.get(runTable.get(i)[3])[2] >runTable.get(i)[1]){
	    			  connectedComponents.get(runTable.get(i)[3])[2]=runTable.get(i)[1];//calculate bounding box left y
	    		  }
	    		  if(connectedComponents.get(runTable.get(i)[3])[3]<0 || connectedComponents.get(runTable.get(i)[3])[3] <runTable.get(i)[0]){
	    			  connectedComponents.get(runTable.get(i)[3])[3]=runTable.get(i)[0];//calculate bounding box right x
	    		  }
	    		  if(connectedComponents.get(runTable.get(i)[3])[4]<0 || connectedComponents.get(runTable.get(i)[3])[4] <runTable.get(i)[2]){
	    			  connectedComponents.get(runTable.get(i)[3])[4]=runTable.get(i)[2];//calculate bounding box right y 
	    		  }
	    	  }
	    	  	double m11=0,m20=0,m02=0;
	    	  for(int i=1;i<runTable.size();i++){
	    		  for(int t=runTable.get(i)[1]-1;t<=runTable.get(i)[2]-1;t++){
	    			  connectedComponents.get(runTable.get(i)[3])[7]=connectedComponents.get(runTable.get(i)[3])[7]+((t-((double)(connectedComponents.get(runTable.get(i)[3])[5])/(double)(connectedComponents.get(runTable.get(i)[3])[0])))*(runTable.get(i)[0]-1-((double)(connectedComponents.get(runTable.get(i)[3])[6])/(double)(connectedComponents.get(runTable.get(i)[3])[0]))));
		    		  connectedComponents.get(runTable.get(i)[3])[8]=connectedComponents.get(runTable.get(i)[3])[8]+Math.pow((t-((double)(connectedComponents.get(runTable.get(i)[3])[5])/(double)(connectedComponents.get(runTable.get(i)[3])[0]))), 2);
		    		  connectedComponents.get(runTable.get(i)[3])[9]=connectedComponents.get(runTable.get(i)[3])[9]+Math.pow((runTable.get(i)[0]-1-((double)(connectedComponents.get(runTable.get(i)[3])[6])/(double)(connectedComponents.get(runTable.get(i)[3])[0]))), 2);
	    		  }
	    	  }
	    	  Scalar [] colors=new Scalar[]{new Scalar(0,0,255),new Scalar(0,255,0),new Scalar(255,0,0),new Scalar(0,255,255),new Scalar(255,0,255)};
	    	  int regionNumber=0;
	    	  String color="";
	    	  for(int h=1;h<connectedComponents.size();h++){
	    		  if(connectedComponents.get(h)[0]>=500){//ignoring regions with less than 500 solid pixels
	    			  color="Purple "+(regionNumber-3);
	    			  if(regionNumber==0){
	    				  color="Red ";
	    			  }
	    			  if(regionNumber==1){
	    				  color="Green ";
	    			  }
	    			  if(regionNumber==2){
	    				  color="Blue ";
	    			  }
	    			  if(regionNumber==3){
	    				  color="Yellow ";
	    			  }
	    			  
	    			  double x=(int)((double)(connectedComponents.get(h)[5])/(double)(connectedComponents.get(h)[0]));//centroid x
	    			  double y=(int)((double)(connectedComponents.get(h)[6])/(double)(connectedComponents.get(h)[0]));// centroid y
	    			  
	    			  m11=connectedComponents.get(h)[7];
	    			  m20=connectedComponents.get(h)[8];
	    			  m02=connectedComponents.get(h)[9];
	    			  double principalAngle=0.5*Math.atan2(2*m11, m20-m02);
	    			  System.out.println("--"+color+"Region--");
	    			  System.out.println("coordinates of centroid: x: "+Math.round((double)(connectedComponents.get(h)[5])/(double)(connectedComponents.get(h)[0]))+" y: "+Math.round((connectedComponents.get(h)[6])/(double)(connectedComponents.get(h)[0])));
	    			  System.out.println("Principal Angle: "+Math.round(Math.abs(-90+Math.toDegrees(principalAngle))));
	    			  if(regionNumber>=4){
	    				  org.opencv.core.Core.line(lenaFinal, new Point(x,y), new Point(x+width*Math.cos(principalAngle),y+width*Math.sin(principalAngle)), colors[4]);
		    			  org.opencv.core.Core.line(lenaFinal, new Point(x+-1*width*Math.cos(principalAngle),y+-1*width*Math.sin(principalAngle)), new Point(x,y),  colors[4]);
		    			   
	    			  }else{
	    				  org.opencv.core.Core.line(lenaFinal, new Point(x,y), new Point(x+width*Math.cos(principalAngle),y+width*Math.sin(principalAngle)), colors[regionNumber]);
	    				  org.opencv.core.Core.line(lenaFinal, new Point(x+-1*width*Math.cos(principalAngle),y+-1*width*Math.sin(principalAngle)), new Point(x,y),  colors[regionNumber]);
	    			  }
	    			  for(int i=(int)(connectedComponents.get(h)[1])-1;i<connectedComponents.get(h)[3];i++){
	    				  for(int j=(int)(connectedComponents.get(h)[2])-1;j<connectedComponents.get(h)[4];j++){
	    					  if((i>=y-3 && i<=y+3 && j==x) || (i==y && j>=x-3 && j<=x+3)){
	    						  lenaFinal.put(i, j, new double[]{0,0,255});// draw centroid
	    					  }
	    					  
	    				  }
	    			  }
	    			  regionNumber++;
	    		  }
	    	  }
	    	  
	    	  
	    	  boolean d=Highgui.imwrite("Objects-"+file.getName(), lenaFinal);
	    	  if(!d){
	    		  System.out.println("Error saving images");
	    	  }
	    	  
	    	  
	      }
	      else
	      {
	    	  System.out.println("Error in the Image format");
	      }
       }
	
	public static int makeEquivalent(ArrayList<int[]> label,ArrayList<int[]> eqClass,int i1,int i2){
		if(label.get(i1)[0]==0 && label.get(i2)[0]==0){
			label.get(i1)[0]=i1;
			label.get(i2)[0]=i1;
			label.get(i1)[1]=i2;
			label.get(i2)[1]=0;
			eqClass.get(i1)[0]=i1;
			return 1;
		}
	
	if(label.get(i1)[0]==label.get(i2)[0]){
		return 1;
	}
	if(label.get(i1)[0]!=0 && label.get(i2)[0]==0){
		int beginning=label.get(i1)[0];
		label.get(i2)[0]=beginning;
		label.get(i2)[1]=eqClass.get(beginning)[0];
		eqClass.get(beginning)[0]=i2;
		return 1;
	}
	if(label.get(i1)[0]==0 && label.get(i2)[0]!=0){
		int beginning=label.get(i2)[0];
		label.get(i1)[0]=beginning;
		label.get(i1)[1]=eqClass.get(beginning)[0];
		eqClass.get(beginning)[0]=i1;
		return 1;
	}
	if(label.get(i1)[0]!=0 && label.get(i2)[0]!=0){
		int beginning=label.get(i2)[0];
		int member=eqClass.get(beginning)[0];
		int eqLabel=label.get(i1)[0];
		while(label.get(member)[1]!=0){
			label.get(member)[0]=eqLabel;
			member=label.get(member)[1];
		}
		label.get(member)[0]=eqLabel;;
		label.get(member)[1]=eqClass.get(eqLabel)[0];
		eqClass.get(eqLabel)[0]=eqClass.get(beginning)[0];
		
		eqClass.get(beginning)[0]=0;
		return 1;
	}
	return 1;
}

	public static void runLengthImplementation(ArrayList<int[]> runTable,ArrayList<int[]> runTableReference,ArrayList<int[]> label,ArrayList<int[]> eqClass){
		int labels=1;
		for(int i=1;i<runTableReference.size();i++){
			int p=runTableReference.get(i)[0];
			int pLast=runTableReference.get(i)[1];
			int q,qLast;
			if(i==1){
				q=0;
				qLast=0;
			}else{
				q=runTableReference.get(i-1)[0];
				qLast=runTableReference.get(i-1)[1];
			}
			
			if(p!=0 && q!=0){
				while(p<=pLast && q<=qLast){
					if(runTable.get(p)[2]<runTable.get(q)[1]){
						p++;
					}else{
						if(runTable.get(q)[2]<runTable.get(p)[1]){
							q++;
						}else{
							int pLabel=runTable.get(p)[3];
							if(pLabel==0){
								runTable.get(p)[3]=runTable.get(q)[3];
							}else{
								if(pLabel!=0 && runTable.get(q)[3]!=pLabel){
									makeEquivalent(label,eqClass,pLabel,runTable.get(q)[3]);
								}
							}
							if(runTable.get(p)[2]>runTable.get(q)[2]){
								q++;
							}else{
								if(runTable.get(q)[2]>runTable.get(p)[2]){
									p++;
								}else{
									if(runTable.get(p)[2]==runTable.get(q)[2]){
										q++;
										p++;
									}
								}
							}
						}
					}
				}
				
			}
			p=runTableReference.get(i)[0];
			if(p!=0){
			
				while(p<=pLast){
					int pLabel=runTable.get(p)[3];
					if(pLabel==0){
						runTable.get(p)[3]=labels;
						labels++;
						
					}else{
						if(pLabel!=0 && label.get(pLabel)[0]!=0 ){
							runTable.get(p)[3]=label.get(pLabel)[0];
						}
					}
					p++;
				}
			}
		}
			int p,pLast,q,qLast;
				for(int j=runTableReference.size()-1;j>=1;j--){
						p=runTableReference.get(j)[0];
						pLast=runTableReference.get(j)[1];
						if(j==runTableReference.size()-1){
							q=0;
							qLast=0;
						}else{
							q=runTableReference.get(j+1)[0];
							qLast=runTableReference.get(j+1)[1];
						}
						if(p!=0 && q!=0){
							while(p<=pLast && q<=qLast){
								if(runTable.get(p)[2]<runTable.get(q)[1]){
									p++;
								}else{
									if(runTable.get(q)[2]<runTable.get(p)[1]){
										q++;
									}else{
										if(runTable.get(p)[3]!=runTable.get(q)[3]){
											label.get(runTable.get(p)[3])[0]=runTable.get(q)[3];
											runTable.get(p)[3]=runTable.get(q)[3];
										}
										if(runTable.get(p)[2]>runTable.get(q)[2]){
											q++;
										}else{
											if(runTable.get(q)[2]>runTable.get(p)[2]){
												p++;
											}else{
												if(runTable.get(q)[2]==runTable.get(p)[2]){
													q++;
													p++;
												}
											}
										}
									}
								}
							}
							}
						p=runTableReference.get(j)[0];
						if(p!=0){
							
							while(p<=pLast){
								if(label.get(runTable.get(p)[3])[0]!=0){
									runTable.get(p)[3]=label.get(runTable.get(p)[3])[0];
								}
								p++;
							}
						}
				}
	}
	}
		
		
	
	
