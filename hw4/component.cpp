#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <vector>
#include <queue>

using namespace std;
using namespace cv;

typedef vector<Point> component;

struct position {
    double x, y, ag;
    position(double _x, double _y, double _ag) :
        x(_x), y(_y), ag(_ag) {}
};

struct analyzer {

    static const int MXC = 1000;
    static const int MIN_COMP_SZ = 30;
    static const int SCALE = 100;

    bool used[MXC][MXC];

    bool valid(Point &p, Mat &mat){
        return p.x >= 0 && p.y >= 0 && p.x < mat.rows && p.y < mat.cols;
    }

    vector<component> comps;
    void bfs(int x, int y, Mat &input){

        static const int d[4][2]={{1,0},{-1,0},{0,1},{0,-1}};

        assert(input.rows < MXC && input.cols < MXC);

        queue<Point> que;
        vector<Point> comp;
        que.push(Point(x, y));
        comp.push_back(Point(x, y));
        used[x][y] = true;

        while (!que.empty()) {

            Point cur = que.front();
            que.pop();

            for (int i = 0; i < 4; i++) {
                Point nex(cur.x + d[i][0], cur.y + d[i][1]);
                if (valid(nex, input) && 
                        !used[nex.x][nex.y] &&
                        input.at<Vec3b>(nex.x, nex.y).val[0] > 0) {
                    que.push(nex);
                    comp.push_back(nex);
                    used[nex.x][nex.y] = true;
                }
            }

        }

        if (comp.size() >= MIN_COMP_SZ) {
            comps.push_back(comp);
        } else {
            for (int i = 0; i < (int)comp.size(); i++) {
                input.at<Vec3b>(comp[i].x, comp[i].y).val[0] = 0;
                input.at<Vec3b>(comp[i].x, comp[i].y).val[1] = 0;
                input.at<Vec3b>(comp[i].x, comp[i].y).val[2] = 0;
            }
        }

    }

    double calc_mo(int xe, int ye, const component &comp) {
        double mo = 0;
        for (int i = 0; i < (int)comp.size(); i++)
            mo += pow(comp[i].x, xe) * pow(comp[i].y, ye);
        return mo;
    }

    double calc_mu(int xe, int ye, const component &comp) {

        double xc = 0, yc = 0;
        for (int i = 0; i < (int)comp.size(); i++) {
            xc += comp[i].x;
            yc += comp[i].y;
        }
        xc /= comp.size();
        yc /= comp.size();

        double mu = 0;
        for (int i = 0; i < (int)comp.size(); i++)
            mu += pow(comp[i].x - xc, xe) * pow(comp[i].y - yc, ye);
        return mu;

    }

    double calc_ag(const component &comp) {
        double mu11 = calc_mu(1, 1, comp);
        double mu20 = calc_mu(2, 0, comp);
        double mu02 = calc_mu(0, 2, comp);
        return atan2(2 * mu11, mu20 - mu02) / 2;
    }

    vector<position> get_pos(const Mat &image) {

        Mat result;
        assert(image.rows < MXC && image.cols < MXC);
        threshold(image, result, 120, 256, THRESH_BINARY);
        for (int i = 0; i < result.rows; i++)
            for (int j = 0; j < result.cols; j++)
                if (!used[i][j] && result.at<Vec3b>(i, j).val[0] > 0)
                    bfs(i, j, result);
        fprintf(stderr, "cnt=%d\n", (int)comps.size());

        vector<position> ret;
        Scalar color(255, 0, 0);
        for (int i = 0; i < (int)comps.size(); i++) {
            component &comp = comps[i];
            double area = calc_mo(0, 0, comp);
            assert(area > 0);
            double xc = calc_mo(1, 0, comp) / area;
            double yc = calc_mo(0, 1, comp) / area;
            circle(result, Point(yc, xc), 4, color, -1, 8, 0);
            double ag = calc_ag(comp);
            double dx = cos(ag) * SCALE;
            double dy = sin(ag) * SCALE;
            line(result, Point(yc - dy, xc - dx), Point(yc + dy, xc + dx), color);
            ret.push_back(position(xc, yc, ag));
        }

        imshow("Display window",result);
        waitKey(0);

        return ret;

    }

};


int main(int argc, char** args) {

	if (argc < 2) {
        fprintf(stderr, "Usage: %s <process_image_path>\n", args[0]);
		return EXIT_FAILURE;
	}

    static analyzer az;
	Mat image = imread(args[1]);
    vector<position> pos = az.get_pos(image);
    for (int i = 0; i < (int)pos.size(); i++) {
        fprintf(stderr, "%f %f %f\n", pos[i].x, pos[i].y, pos[i].ag);
    }

}
